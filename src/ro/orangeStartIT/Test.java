package ro.orangeStartIT;

import java.util.Arrays;
import java.util.List;

public class Test {

//Build an exercise using lambda expressions where you will filter a particular employee.
//For that, you will implement two classes: Employee and Test.

    /*
    •
    • Convert the employees to stream
    • You will filter only one employee
    • If findAny(), then return found orElse(), return null
    • Print and check if you got the employee you wanted
     */
    public static void main(String[] args) {
    List<Employee> employees =  Arrays.asList( // A list called Employee and create a new ArrayList
        new Employee("Silviu", 34),
        new Employee("Stefan", 13),
        new Employee("Andrei", 16));//Three objects of type Employee with attributes

    Employee employee = employees.stream().filter(e -> e.getAge()<17).findAny().orElse(null);
        System.out.println(employee.getName());
    }
}
