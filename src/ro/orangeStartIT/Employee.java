package ro.orangeStartIT;
//The Employee class will have:
public class Employee {
    private String name;
    private int age; //Two instance variables: name, age

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }//Getters and setters

    public Employee(String name, int age) {
        this.name = name;
        this.age = age; //Proper constructor
    }
    public String toString(){
    //Override toString() method in class to print proper output and return the name and age.
            return "Employee name: " + getName() + " is " + getAge() + "old.";
    }
}